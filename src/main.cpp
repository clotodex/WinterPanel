#define LOG_FILE_NAME "winterpanel.log"

/* for printf */
#include <stdio.h>
#include <stdarg.h>
/* for the return value of main */
#include <stdlib.h>

/* to open/close filedescriptors */
#include <unistd.h>
#ifdef LOG_FILE_NAME
	#include <fcntl.h>
#endif

/* reading command input */
#include <string>
#include <pstreams/pstream.h>
#include <sstream>

/* signal handling */
#include <signal.h>

bool quit_signal = false;

void log(const char* message, ...)
{
#ifdef LOG_FILE_NAME
	int fd = open(LOG_FILE_NAME, (O_WRONLY | O_CREAT | O_APPEND), 0655);
#else
	int fd = STDOUT_FILENO;
#endif

	dprintf(fd, "[LOG] ");

	va_list argptr;
	va_start(argptr, message);

	vdprintf(fd, message, argptr);

	va_end(argptr);

#ifdef LOG_FILE_NAME
	close(fd);
#endif
}
void debug(const char* message, ...)
{
#ifdef LOG_FILE_NAME
	int fd = open(LOG_FILE_NAME, (O_WRONLY | O_CREAT | O_APPEND), 0655);
#else
	int fd = STDOUT_FILENO;
#endif

	dprintf(fd, "[DEBUG] ");

	va_list argptr;
	va_start(argptr, message);

	vdprintf(fd, message, argptr);

	va_end(argptr);
#ifdef LOG_FILE_NAME
	close(fd);
#endif
}
void error(const char* message, ...)
{
#ifdef LOG_FILE_NAME
	int fd = open(LOG_FILE_NAME, (O_WRONLY | O_CREAT | O_APPEND), 0655);
#else
	int fd = STDERR_FILENO;
#endif

	dprintf(fd, "[ERROR] ");

	va_list argptr;
	va_start(argptr, message);

	vdprintf(fd, message, argptr);

	va_end(argptr);
#ifdef LOG_FILE_NAME
	close(fd);
#endif
}


void cleanup()
{
	//TODO is destructor of pstream enaugh? - should it be called here?
	//cleanup open streams, filedescriptors
}

void shutdown(const char* message, int code)
{
	error("[SHUTDOWN] %s\n", message);
	cleanup();
	exit(code);
}

void shutdown_signal(int s)
{
	error("Caught signal %d\n", s);
	quit_signal = true;
}

void handle_line(const std::string& line)
{
	if (quit_signal)
		shutdown("Received signal_quit", EXIT_FAILURE);

	std::stringstream ss;
	ss.str(line);
	std::string item;

	if (!std::getline(ss, item, '\t'))
	{
		error("command has no first element");
		return;
	}

	if (item == "[wp]")
	{
		item.clear();
		if (!std::getline(ss, item, '\t'))
		{
			error("protocol error: [wp] has to be followed by something\n");
			return;
		}

		if (item == "event")
		{
			//TODO
		}
		else if (item == "reload")
		{
			//TODO if reload panel - restart everything if this is supposed to be supported
		}
		else if (item == "request")
		{
			//TODO handle request (types: sheduling, ...?)
			//TODO fill timing array & stop and start sheduler
			//sheduler is background task? stop and start for new timings
		}
		else if (item == "ping")
		{
			//TODO herbstclient emit_hook "[wp]" "pong" "${parts[2]}"
		}
		else if (item == "pong" || item == "confirm" || item == "action" || item == "quit")
		{
			//sent by the panel so just ignore them - maybe redefine protocol to have component channel and panel channel and filter it out by regex
		}
		else
			error("Unknown protocol action: %s\n", item.data());
	}
	else if (item == "reload" || item == "quit_panel")
		shutdown("quit requested", EXIT_SUCCESS);
	else
		//ignore - command not found or not intended for handling - should not happen if regex is correct
		error("'%s' should have been ignored by the regex\n", item.data());
}

void herbstclient_listen()
{
	// run a process and create a streambuf that reads its stdout and stderr
	// listen for herbstluft events with a regex filtering out unnecessary stuff
	redi::ipstream proc("herbstclient --idle  \"(\\[wp\\].*|reload|quit_panel)\"", redi::pstreams::pstdout | redi::pstreams::pstderr);
	std::string line;
	// read child's stdout
	while (std::getline(proc.out(), line))
		handle_line(line);
	// read child's stderr
	while (std::getline(proc.err(), line))
		error("[HERBSTCLIENT] %s\n", line.data());
}

int main()
{
	log("Starting up Winterpanel...\n");
	log("Setting signal-handler... ");
	struct sigaction sa;
	sa.sa_handler = shutdown_signal;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART; /* Restart functions if interrupted by handler */
	if (sigaction(SIGINT, &sa, nullptr) == -1)
		shutdown("Could not install signal-handler", EXIT_FAILURE);
	log("DONE\n");

	log("Startup successful!");

	log("Listening for herbstluft events");
	herbstclient_listen();
	return EXIT_SUCCESS;
}
