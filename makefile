TARGETS := MAIN

#target
TARGET_MAIN               := WinterPanel
TARGET_MAIN_SOURCE_DIRS   := src

#compiler flags
TARGET_MAIN_CFLAGS        = -O3 -flto=${CORES} -std=c++1z -Wall -Wextra -Weffc++
TARGET_MAIN_LDFLAGS       = -fuse-linker-plugin
TARGET_MAIN_LDLIBS        = -lz -lpthread

#only appended if file .debug exists
TARGET_MAIN_DEBUG_CFLAGS  = -g -D__DEBUG__
TARGET_MAIN_DEBUG_LDFLAGS = -Wl,-z,now -rdynamic
TARGET_MAIN_DEBUG_LDLIBS  = -ldl -lbfd

include $(shell curator --makefile c++)
