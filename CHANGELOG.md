# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Changelog
- Logger with log/debug/error
- Reading special events from herbstclient
- handling each event in a protocol outline
- mini signal handler calling cleanup method

[Unreleased]: https://gitlab.com/Clotodex/WinterPanel/compare/v.0.0.0...HEAD
